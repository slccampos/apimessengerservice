﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIMessengerService.Models
{
    public class AppDbContext : IdentityDbContext<ApplicationUser>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
               : base(options)
        {
        }
        public DbSet<NormalUser> Users { get; set; }
        public DbSet<ClassicMessage> Messages { get; set; }

    }
}
