﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIMessengerService.Models
{
    interface IReadBehavior
    {
        string readMessages();
    }
}
