﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIMessengerService.Models
{
    public class NormalUser : IUser
    {
        public NormalUser()
        {
            inbox = new List<ClassicMessage>();
        }
        public NormalUser(string name)
        {
            username = name;
            inbox = new List<ClassicMessage>();
        }
        public NormalUser( string name, List<ClassicMessage> inbox)
        {
            username = name;
            this.inbox = inbox;
        }
        public int id { get; set; }
        public string username { get; set; }
        public List<ClassicMessage> inbox { get; set; }
        public string password { get; set; }
    }
}
