﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIMessengerService.Models
{
    interface IMessage
    {
        int id { get; set; }
        string dateSent { get; set; }
        string dateRead { get; set; }
        string transmitterUser { get; set; }
        string receiverUser { get; set; }
        void performRead();
    }
}
