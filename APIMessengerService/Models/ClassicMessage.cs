﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace APIMessengerService.Models
{
    public class ClassicMessage : IMessage
    {
        public ClassicMessage()
        {
            dateSent = DateTime.Now.ToString("G");
            readBehavior = new ReadWithSeen();
        }
        public ClassicMessage(string message, string transmitted, string receiver)
        {
            messageBody = message;
            transmitterUser = transmitted;
            receiverUser = receiver;
            dateSent = DateTime.Now.ToString("G");
            dateRead = null;
            readBehavior = new ReadWithSeen();
        }
        private IReadBehavior readBehavior { get; set; }
        public int id { get; set; }
        public string dateSent { get ; set; }
        public string dateRead { get ; set; }
        public string messageBody { get; set; }
        [ForeignKey("NormalUser")]
        public string transmitterUser { get; set; }
        [NotMapped,JsonIgnore]
        public NormalUser transmitter { get; set; }
        [ForeignKey("NormalUser")]
        public string receiverUser { get; set; }
        [NotMapped,JsonIgnore]
        public NormalUser receiver { get; set; }

        public void performRead()
        {
            dateRead = readBehavior.readMessages();
        }
    }
}
