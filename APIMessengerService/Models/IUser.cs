﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIMessengerService.Models
{
    interface IUser
    {
        int id { get; set; }
        string username { get; set; }
        string password { get; set; }
        List<ClassicMessage> inbox { get; set; }
        
    }
}
