﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIMessengerService.Models
{
    public class ReadWithSeen : IReadBehavior
    {
        public string readMessages()
        {
            return DateTime.Now.ToString("G");
        }
    }
}
