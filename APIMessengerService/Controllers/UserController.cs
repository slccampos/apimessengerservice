﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using APIMessengerService.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace APIMessengerService.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    
    public class UserController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _context;

        public UserController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IConfiguration configuration,
            AppDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _context = context;
        }

        [HttpGet]
        [Route("GetAll")]//http://localhost:50721/api/user/getall
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IEnumerable<NormalUser> Get()
        {
            return _context.Users.ToList();
        }

        [HttpGet("{username}", Name = "userCreated")]
        [Route("GetThis")]//http://localhost:50721/api/user/GetThis?username=prueba1@gmail.com
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetById(string userName)
        {
            var user = _context.Users.Include(x => x.inbox).FirstOrDefault(x => x.username == userName);
            if (user == null)
            {
                return NotFound(); //404 Not found
            }
            return Ok(user); //302 Found

        }

        //[HttpPost(Name = "createUser")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //public IActionResult Post ([FromBody] NormalUser user)
        //{
        //    if (ModelState.IsValid) // user != null
        //    {
        //        _context.Users.Add(user);
        //        _context.SaveChanges();
        //        return new CreatedAtRouteResult("userCreated", new {user.id}, user); //201 Created
        //    }
        //    return BadRequest(ModelState); //400 Bad Request
        //}

        [HttpPut("{id}")]
        [Route("Modify")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Put([FromBody] NormalUser user, int id)
        {
            if (user == null || user.id != id)
            {
                return BadRequest();

            }
            _context.Entry(user).State = EntityState.Modified;
            _context.SaveChanges();
            return NoContent(); //No Content 204
        }

        [HttpDelete("{id}")]
        [Route("Delete")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Delete(int id)
        {
            var user = _context.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }
            _context.Users.Remove(user);
            _context.SaveChanges();
            return Ok(user);
        }

        [HttpPost]
        [Route("Create")]//http://localhost:50721/api/user/create
        public async Task<IActionResult> CreateUser([FromBody] NormalUser model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.username, Email = model.username };
                var result = await _userManager.CreateAsync(user, model.password);
                if (result.Succeeded)
                {
                    _context.Users.Add(model);
                    _context.SaveChanges();
                    return BuildToken(model);
                }
                else
                {
                    return BadRequest("Username or password invalid");
                }
            }
            else
            {
                return BadRequest(ModelState);
            }

        }

        [HttpPost]
        [Route("Login")]//http://localhost:50721/api/user/login
        public async Task<IActionResult> Login([FromBody] NormalUser userInfo)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(userInfo.username, userInfo.password, isPersistent: false, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    return BuildToken(userInfo);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return BadRequest(ModelState);
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        private IActionResult BuildToken(IUser userInfo)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, userInfo.username),
                new Claim("miValor", "Lo que yo quiera"),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["secret_key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expiration = DateTime.UtcNow.AddHours(1);

            JwtSecurityToken token = new JwtSecurityToken(
               issuer: "yourdomain.com",
               audience: "yourdomain.com",
               claims: claims,
               expires: expiration,
               signingCredentials: creds);

            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration = expiration
            });

        }

    }
}