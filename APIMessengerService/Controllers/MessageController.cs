﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using APIMessengerService.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.HttpSys;
using Microsoft.EntityFrameworkCore;

namespace APIMessengerService.Controllers
{
    [Produces("application/json")]
    [Route("api/User/{UserName}/Message")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MessageController : Controller
    {
        private readonly AppDbContext _context;

        public MessageController(AppDbContext _context)
        {
            this._context = _context;
        }

        [HttpGet]
        [Route("GetAll")] //http://localhost:50721/api/user/test2@gmail.com/message/getall
        public IEnumerable<ClassicMessage> GetAll(string userName)
        {
            IEnumerable<ClassicMessage> messagesWithoutRead = _context.Messages.Where(x => x.receiverUser == userName).ToList();
            CheckMessagesForRead(messagesWithoutRead);
            return _context.Messages.Where(x => x.receiverUser == userName).ToList();
        }

        private void CheckMessagesForRead(IEnumerable<ClassicMessage> messagesWithoutRead)
        {
            foreach (var message in messagesWithoutRead)
            {
                if (message.dateRead == null)
                {
                    message.performRead();
                    _context.Entry(message).State = EntityState.Modified;
                    _context.SaveChanges();
                }
            }
        }

        [HttpGet("{id}", Name = "messageById")]
        [Route("GetThis")]//http://localhost:50721/api/user/test2@gmail.com/message/getthis?id=2
        public IActionResult GetById(string userName,int id)
        {
            var message = _context.Messages.FirstOrDefault(x => x.id == id && x.receiverUser == userName);
            if (message == null)
            {
                return NotFound(); 
            }
            return new ObjectResult(message); //Ok(message);

        }

        [HttpPost]
        [Route("Create")] //http://localhost:50721/api/user/test2@gmail.com/message/create
        public IActionResult Create([FromBody] ClassicMessage message)
        {
            if (ModelState.IsValid) 
            {
                _context.Messages.Add(message);
                _context.SaveChanges();
                return new CreatedAtRouteResult("messageById", new { message.id }, message);
            }
            return BadRequest(ModelState); //400 Bad Request
        }

        [HttpPut("{id}")]
        [Route("Modify")]
        public IActionResult Put([FromBody] ClassicMessage message, int id)
        {
            if (message == null || message.id != id)
            {
                return BadRequest();
            }
            _context.Entry(message).State = EntityState.Modified;
            _context.SaveChanges();
            return NoContent();
        }

        [HttpDelete("{id}")]
        [Route("Delete")]
        public IActionResult Delete(int id)
        {
            var message = _context.Messages.Find(id);
            if (message == null)
            {
                return NotFound();
            }
            _context.Messages.Remove(message);
            _context.SaveChanges();
            return Ok(message);
        }

    }
}