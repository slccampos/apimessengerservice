# README #

## What is this repository for? ##
REST service that allows external applications to create and manage message lists.
Service allow users to be registered, send messages between them and retrieve all the messages directed to a specific user.
## How do I get set up? ##
You need: 

- **.NET CORE 2.0**
- **SQLSERVER**
- **POSTMAN**
- to avoid complications of preference **MICROSOFT VISUAL STUDIO 2017**
## Getting started ##
Now that you cloned it, if you do not have the default instance in sqlserver "mssqllocaldb"  Open *appsettings.json* and change to the instance name
Create a database with the name "ApiMessengerService"; otherwise, open the *appsetings.json* file and make the corresponding changes.  
check in the **sql server object browser**  
**open Package Manager Console**  
`PM> update-database`  
If you have error > A network-related or instance-specific while establishing a connection to SQL Server and persists.
In the query window execute.
```sql
use ApiMessengerService
EXEC sp_configure 'remote access', 0 ;  
GO  
RECONFIGURE;  
GO
```
restart instance, if persists, disconnect instance and restart service: SQL Server (MSSQLSERVER)
## How does this work  ##
**(POST)Create user:**

- http://localhost:port/api/user/create  

in postman, choose post, then check raw, then JSON and then write on the box:  
```json
{
	"username": "customer@email.com",
	"password": "password"
}
```
*From here we need the token that returns us the* `create`  
Then to use the token in the `Authorization` tab choose the `Bearer Token` type and paste the token and then click on `Preview Request`  
**(GET)Get user:**  

- http://localhost:port/api/user/GetThis?username=customer@email.com  

**(GET)Get all messages for the user:**  

- http://localhost:port/api/user/prueba1@email.com/message/getall  

**(POST)Send message another user**  

- http://localhost:port/api/user/test2@email.com/message/create
```json
{
	"messageBody": "Hey how are you? ",
	"transmitterUser": "customer@email.com",
	"receiverUser": "anothercustomer@email.com"
}
```
**(GET)Get a specific message**  

- http://localhost:port/api/user/customer@email.com/message/getthis?id=9085673